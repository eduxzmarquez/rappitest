import React, { Component } from 'react'
import { connect } from 'react-redux'
import Header from '../../components/header'
import ItemGroupComponent from '../../components/ItemGroupComponent'
//ACTIONS
import { setRank } from '../../redux/actions/products'

class Home extends Component {
  componentWillUnmount(){
    this.props.setRankOfProducts(-1)
  }

  render() {
    return (
      <>
        <Header categories={this.props.categories} />
        <ItemGroupComponent products={this.props.productsListed} />
      </>
    )
  }
}

const mapStateToProps = ({ productsReducer: { productsListed, products, categories } }) => ({
  productsListed,
  categories,
  products
})

const mapDispatchToProps = (dispatch) => ({
  setRankOfProducts: (number) => (dispatch(setRank(number))),
})
export default connect(mapStateToProps, mapDispatchToProps)(Home)
