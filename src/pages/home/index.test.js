import React from 'react';
import ReactDOM from 'react-dom';
import Home from './index';

import { store } from '../../redux/store'

it('Home: renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Home store={store} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
