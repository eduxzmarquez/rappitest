import React from 'react';
import ReactDOM from 'react-dom';
import ShoppingCart from './index';

import { store } from '../../redux/store'

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ShoppingCart store={store} />, div);
  ReactDOM.unmountComponentAtNode(div);
});