import React, { Component } from 'react'
import { connect } from 'react-redux'


import { setRank } from '../../redux/actions/products'
import TableShoppingCart from '../../components/tableShoppingCart'
import Header from '../../components/header'

import { editProductUnit, removeProduct } from '../../redux/actions/products'

class ShoppingCart extends Component {
    constructor(props) {
        super(props)
        this.handleEdit = this.handleEdit.bind(this)
        this.handleRemove = this.handleRemove.bind(this)
    }

    componentWillUnmount(){
        this.props.setRankOfProducts(-1)
    }
    
    _preparingProductsItems() {
        return this.props.shoppingCartItems.map(item => {
            let prepared = this.props.products.find(product => {
                return product.id === item.id
            })
            return { ...prepared, ...item }
        })
    }

    handleEdit({ item, value }) {
        this.props.editProduct(item, value)
    }
    handleRemove(item) {
        this.props.removeProduct(item)
    }

    render() {
        let shoppingCartItems = this._preparingProductsItems(this.props.shoppingCartItems)
        return (
            <div>
                <Header categories={this.props.categories} />
                <TableShoppingCart
                    products={shoppingCartItems}
                    handleEdit={this.handleEdit}
                    handleDelete={this.handleRemove}
                />
            </div>
        )
    }
}

const mapStateToProps = ({ productsReducer: { shoppingCartItems, categories, products } }) => ({
    shoppingCartItems,
    categories,
    products
})

const mapDispatchToProps = (dispatch) => ({//removeProduct
    editProduct: (item, value) => dispatch(editProductUnit(item, value)),
    removeProduct: (item) => dispatch(removeProduct(item)),
    setRankOfProducts: (number) => (dispatch(setRank(number))),
})

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart)
