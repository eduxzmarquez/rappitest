import React from 'react';


//REDUX && ROUTER
import { Provider } from 'react-redux'
import { store, history } from './redux/store'
import { Router, Route } from 'react-router'

//ROUTES
import Home from './pages/home'
import ShoppingCart from './pages/shoppingCart'


function AppRouter() {
  return (
    <>
      <Provider store={store}>
        <Router history={history}>
          <Route exact path='/cart' component={ShoppingCart} />
          <Route exact path='/' component={Home} />
        </Router>
      </Provider>
    </>
  );
}

export default AppRouter;
