import React from 'react';
import ReactDOM from 'react-dom';
import SingleItemComponent from './index';

const product = {
    "quantity": 891,
    "price": "$5,450",
    "available": true,
    "sublevel_id": 3,
    "name": "mollit",
    "id": "58b5a5b117bf36cf8aed54ab"
  }

it('TableShoppingCart: renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SingleItemComponent data={product} />, div);
  ReactDOM.unmountComponentAtNode(div);
});