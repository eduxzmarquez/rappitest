import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import ProductImage from '../../assets/images/buzon_vacio.png'
import FormDialogUnit from '../formDialogUnit'

import { store } from '../../redux/store'
import { addToCart } from '../../redux/actions/products'

const useStyles = makeStyles({
  card: {
    maxWidth: 260,
    margin: 10
  },
  cardDisable: {
    backgroundColor: "rgba(232,21,42,0.5)"
  },
  growHight: {
    flexGrow: 2
  },
  growLow: {
    flexGrow: 1
  },
  media: {
    height: 140,
  },
  textCenter: {
    justifyContent: "center"
  }
});

function SingleItemComponent(props) {
  const classes = useStyles();
  const { data } = props;

  const [Modal, setModal] = React.useState(false);
  
  const handleBuyProduct = (number) => {
    if(number > 0)store.dispatch(addToCart(data, Number.parseInt(number)))
    handleCloseModal()
  }
  const handleOpenModal = () => {
    setModal(true)
  }
  const handleCloseModal = () => {
    setModal(false)
  }
  const disable = data.available ? '' : classes.cardDisable
  const classCard = classes.card + " " + disable
  return (
    <Card spacing={5} className={classCard}>
      <FormDialogUnit
        handleUnitValue={handleBuyProduct.bind(this)}
        open={Modal}
        close={handleCloseModal}
      />
      <CardActionArea onClick={()=>{handleOpenModal()}}>
        <CardMedia
          className={classes.media}
          image={ProductImage}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2" >
            {data.name}
          </Typography>
          <Typography variant="subtitle1" color="textPrimary" component="p">
            Precio:{data.price}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Cantidad:{data.quantity}
          </Typography>
          {
            !!data.categoryName &&
          <Typography variant="body2" color="textSecondary" component="p">
            Categoria:{data.categoryName}
          </Typography>
          
          }
          <Typography variant="body2" color="textSecondary" component="p">
            Subnivel:{data.sublevel_id}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button onClick={()=>{handleOpenModal()}} size="small" color="primary">
          Comprar
        </Button>
      </CardActions>
    </Card>
  );
}

export default SingleItemComponent;