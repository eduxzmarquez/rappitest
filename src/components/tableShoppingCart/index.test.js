import React from 'react';
import ReactDOM from 'react-dom';
import TableShoppingCart from './index';

const products = [{
    "unit":13,
    "id": "58b5a5b1b6b6c7aacc25b3fb"
  },
  {
      "unit":6,
    "id": "58b5a5b117bf36cf8aed54ab"
  }]

it('TableShoppingCart: renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TableShoppingCart products={products} />, div);
  ReactDOM.unmountComponentAtNode(div);
});