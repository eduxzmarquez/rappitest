import React from 'react';
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles';

// UI
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Hidden from '@material-ui/core/Hidden';

// UI ICONS
import CreditCard from '@material-ui/icons/CreditCard';
import Edit from '@material-ui/icons/Edit';
import DeleteOutline from '@material-ui/icons/DeleteOutline';

import { store } from "../../redux/store"
import { buyAllInCart } from '../../redux/actions/products'

import FormDialogUnit from '../formDialogUnit'
import SimpleModal from '../alertSimple';

const TAX_RATE = 0.10;

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 320,
    },
}));

function ccyFormat(num) {
    return `${num.toFixed(2)}`;
}

function subtotal(items) {
    return items.map(({ mount }) => mount).reduce((sum, i) => sum + i, 0);
}

const _procesingPrice = (cadena = '') => {
    let formattedPrice = Number.parseFloat(cadena.replace("$", '').replace(",", "."))
    return formattedPrice
}


function TableShoppingCart(props) {
    const classes = useStyles();
    const [Modal, setModal] = React.useState(false);
    const [ItemSelected, setItemSelected] = React.useState({});
    const [ModalBuy, setModalBuy] = React.useState(false);
    
    const { handleDelete, handleEdit } = props
    const handleEditFromModal = (value) => {
        if (!!value) handleEdit({ item: ItemSelected, value: Number.parseInt(value) })
        setModal(false)
    }
    const handleRemove = (item) => {
        handleDelete(item)
    }

    const handleOpenModal = (item) => {
        setItemSelected(item)
        setModal(true)
    }
    const handleCloseModal = () => {
        setModal(false)
    }

    const buyAll = () => {
        store.dispatch(buyAllInCart())
        setModalBuy(true)
    }

    const handleCloseModalBuy = ()=>{
        setModalBuy(false)
    }


    const formattedProducts = props.products.map(producto => {
        let formatted = ({ ...producto, price: _procesingPrice(producto.price) })
        formatted.mount = formatted.price * formatted.unit
        return formatted
    })

    const invoiceSubtotal = subtotal(formattedProducts)
    const invoiceTaxes = TAX_RATE * invoiceSubtotal;
    const invoiceTotal = invoiceTaxes + invoiceSubtotal;

    return (
        <>
            <Paper className={classes.root}>
                <SimpleModal open={ModalBuy} handleCloseEvent={handleCloseModalBuy.bind(this)} 
                text={"Tu compra fue hecha satisfactoriamente!!"} title={"Shopping Cart"} />
                <FormDialogUnit
                    handleUnitValue={handleEditFromModal.bind(this)}
                    open={Modal}
                    close={handleCloseModal}

                />
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Acciones</TableCell>
                            <TableCell>Nombre</TableCell>
                            <Hidden only={["xs", "sm"]}>
                                <TableCell align="right">Cantidad</TableCell>
                            </Hidden>
                            <Hidden only={["xs"]}>
                                <TableCell align="right">Unidad</TableCell>
                            </Hidden>
                            <Hidden only={["xs", "sm"]}>
                                <TableCell align="right">Precio</TableCell>
                            </Hidden>
                            <TableCell align="right">Monto</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {formattedProducts.map(row => (
                            <TableRow key={row.name}>
                                <TableCell><>
                                    <DeleteOutline onClick={() => handleRemove(row)} />
                                    <Edit onClick={() => handleOpenModal(row)} />
                                </></TableCell>
                                <TableCell>{row.name}</TableCell>
                                <Hidden only={["xs", "sm"]}>
                                    <TableCell align="right">{row.quantity}</TableCell>
                                </Hidden>
                                <Hidden only={["xs"]}>
                                    <TableCell align="right">{row.unit}</TableCell>
                                </Hidden>

                                <Hidden only={["xs", "sm"]}>
                                    <TableCell align="right">${ccyFormat(row.price)}</TableCell>
                                </Hidden>
                                <TableCell align="right">${ccyFormat(row.mount)}</TableCell>
                            </TableRow>
                        ))}

                    </TableBody>
                </Table>
            </Paper>
            <Paper>

                <Table className={classes.root}>
                    <TableBody>
                        <TableRow>
                            <TableCell rowSpan={3} />
                            <TableCell colSpan={2}>Subtotal</TableCell>
                            <TableCell align="right">${ccyFormat(invoiceSubtotal)}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>Tax</TableCell>
                            <TableCell align="right">{`${(TAX_RATE * 100).toFixed(0)} %`}</TableCell>
                            <TableCell align="right">${ccyFormat(invoiceTaxes)}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={2}>Total</TableCell>
                            <TableCell align="right">${ccyFormat(invoiceTotal)}</TableCell>
                        </TableRow>
                    </TableBody>

                </Table>
                <Box display="flex" justifyContent="flex-end" m={1} p={1}>
                    <Button onClick={() => buyAll()} variant="contained" color="secondary" className={classes.button}>
                        Comprar
                        <CreditCard />
                    </Button>
                </Box>
            </Paper>
        </>
    );
}

TableShoppingCart.propTypes = {
    products: PropTypes.array.isRequired
}


export default TableShoppingCart;