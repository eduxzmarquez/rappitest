import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ImageIcon from '@material-ui/icons/Image';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import PropTypes from 'prop-types'

import { setRank } from '../../redux/actions/products'


/**
 * @param data  -- Es un objeto que representa una categoria.
 * **{
 * ****id:number 
 * ****name:string, 
 * ****subLevels[{ ...data }] 
 * **}
 */

class SingleCollapsingItemComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false
        }
        this.handleClick = this.handleClick.bind(this)
        this.handleSelectCategory = this.handleSelectCategory.bind(this)
    }

    handleClick() {
        this.setState({ open: !this.state.open })
    }

    handleSelectCategory(number){
        this.props.setRankOfProducts(number)
        this.handleClick()
    }

    render() {
        const { data, classes } = this.props
        return (
            <List
                className={classes.root}
            >
                <ListItem button onClick={() => { this.handleSelectCategory(data.id) }}>
                    <ListItemIcon>
                        <ImageIcon />
                    </ListItemIcon>
                    <ListItemText primary={data.name} />
                    {!!data.sublevels && (this.state.open ?
                        <ExpandLess />
                        : <ExpandMore />)}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                    
                    {!!data.sublevels &&
                        data.sublevels.map((category, index) => {
                            return <SingleCollapsingItemComponentWithStyles
                                key={category.id}
                                data={{ ...category, margin: !!data.margin ? data.margin + 10 : 10 }} />
                        })
                    }
                </Collapse>
            </List>
        )
    }
}

const classes = (theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
})

SingleCollapsingItemComponent.propTypes = {
    data: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
}

const mapStateToProps = () => ({})
const mapDispatchToProps = (dispatch) => ({
    setRankOfProducts: (number) => dispatch(setRank(number))
})
const SingleCollapsingItemComponentWithStyles = withStyles(classes)(connect(mapStateToProps,mapDispatchToProps)(SingleCollapsingItemComponent))

export default SingleCollapsingItemComponentWithStyles;