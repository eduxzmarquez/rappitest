import React from 'react';
import ReactDOM from 'react-dom';
import SingleCollapsingItemComponentWithStyles from './index';
import { store } from '../../redux/store'
const category= {
    "id": 1,
    "name": "Gaseosas",
    "sublevels": [
        {
            "id": 2,
            "name": "Con azúcar"
        },
        {
            "id": 3,
            "name": "Sin azúcar"
        }
    ]
}

it('SingleCollapsingItemComponentWithStyles: renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SingleCollapsingItemComponentWithStyles store={store} data={category} />, div);
  ReactDOM.unmountComponentAtNode(div);
});