import React from 'react';
import ReactDOM from 'react-dom';
import ItemGroupComponent from './index';

import { products } from "../../data/products.json"

it('ItemGroupComponent: renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ItemGroupComponent products={products} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
