import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FilterListIcon from '@material-ui/icons/FilterList';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import Grid from '@material-ui/core/Grid';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';

import Collapse from '@material-ui/core/Collapse';

import TextField from '@material-ui/core/TextField';

import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';



import SingleItemComponent from '../singleItemComponent'

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  })
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const headRows = [
  { id: 'price', numeric: true, disablePadding: false, label: 'Precio' },
  { id: 'quantity', numeric: true, disablePadding: false, label: 'Cantidad' },
  { id: 'available', numeric: true, disablePadding: false, label: 'Disponible' },
];

function ItemGroupComponentHead(props) {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headRows.map(row => (
          <TableCell
            key={row.id}
            align={"center"}
            padding={row.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === row.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === row.id}
              direction={order}
              onClick={createSortHandler(row.id)}
            >
              {row.label}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

ItemGroupComponentHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};


// -------------  START STYLES -----------------

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
}));


//-----------------END STYLES ------------------

const ItemGroupComponentToolbar = props => {
  const classes = useToolbarStyles();
  const { name, toggleCollapse } = props;

  return (
    <Toolbar
      className={classes.root}
    >
      <div className={classes.title}>
        <Typography variant="h6" id="tableTitle">
          {name}
        </Typography>
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        <Tooltip title={`Lista de ${name}`}>
          <IconButton onClick={toggleCollapse} aria-label={`Lista de ${name}`}>
            <FilterListIcon />
          </IconButton>
        </Tooltip>
      </div>
    </Toolbar>
  );
};

ItemGroupComponentToolbar.propTypes = {
  name: PropTypes.string.isRequired,
};


//---------------START STYLES --------------------
const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 350,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  form: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
  }
}));

const StylesGrid = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing(2),
  },
}));

//--------------------END STYLES ----------------
function ItemGroupComponent(props) {
  const classes = useStyles();
  const classesGrid = StylesGrid();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [available, setAvailable] = React.useState(0);
  const [min, setMin] = React.useState(0);
  const [max, setMax] = React.useState(0);
  const [cantidad, setCantidad] = React.useState(0);
  const [nameProduct, setNameProduct] = React.useState('');
  const [ToggleFilter, setToggleFilter] = React.useState(false);
  const [ToggleOrder, setToggleOrder] = React.useState(false);

  const _procesingPrice = (cadena = '') => {
    let formattedPrice = Number.parseFloat(cadena.replace("$", '').replace(",", "."))
    return formattedPrice
  }


  function handleRequestSort(event, property) {
    const isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  }

  function handleSelectAllClick(event) {
    if (event.target.checked) {
      const newSelecteds = props.products.map(n => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  }


  const handleChange = (event) => {
    setAvailable(
      Number.parseInt(event.target.value)
    );
  }

  const handleChangeMin = event => {
    setMin(event.target.value)
  };
  const handleChangeMax = event => {
    setMax(event.target.value)
  };

  const handleChangeQuantity = event => {
    setCantidad(event.target.value)
  };

  const handleChangeNameProduct = event => {
    setNameProduct(event.target.value)
  }

  const handleChangeToggleFilter = event => {
    setToggleFilter(!ToggleFilter)
  };

  const handleChangeToggleOrder = event => {
    setToggleOrder(!ToggleOrder)
  };


  let newProducts = props.products;

  if( !!nameProduct ){
    newProducts = props.products.filter(product => {      
      return product.name.search(nameProduct) !== -1 ? true : false
    }).sort((a, b) =>{
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    })
  }
  if (!!min && !!max && Number.parseFloat(min) > 0 && Number.parseFloat(min) < Number.parseFloat(max)) {
    newProducts = props.products.filter(product => {
      let price = _procesingPrice(product.price)
      return price >= Number.parseFloat(min) && price <= Number.parseFloat(max)
    })
  }
  if (!!cantidad && cantidad > 0) {
    newProducts = newProducts.filter(product => {
      return product.quantity <= cantidad
    })
  }
  if (!!available) {
    let formattedAvailable = available === 1 ? true : false
    newProducts = newProducts.filter(product => {
      return product.available === formattedAvailable
    })
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper} >

        <ItemGroupComponentToolbar name={"Filtrado"} toggleCollapse={handleChangeToggleFilter} />
        <Collapse in={ToggleFilter} timeout="auto" unmountOnExit>

          <form className={classes.form} autoComplete="off">
            <FormControl className={classes.margin} >
              <Typography variant="h6" id="body2">
                Rango de Precio
        </Typography>

              <TextField
                id="outlined-min"
                variant="outlined"
                placeholder="Precio Minimo"
                className={classes.textField}
                value={min}
                onChange={handleChangeMin}
                margin="normal"
                InputProps={{
                  startAdornment: <InputAdornment position="start">$</InputAdornment>,
                }}
              />

              <TextField
                id="outlined-max"
                placeholder="Precio Maximo"
                className={classes.textField}
                value={max}
                onChange={handleChangeMax}
                margin="normal"
                variant="outlined"
                InputProps={{
                  startAdornment: <InputAdornment position="start">$</InputAdornment>,
                }}
              />

            </FormControl>
            <FormControl className={classes.margin}>
              <Typography variant="h6" id="body2">
                Stock
            </Typography>
              <TextField
                id="outlined-max"
                placeholder="Cantidad Minima"
                className={classes.textField}
                value={cantidad}
                onChange={handleChangeQuantity}
                margin="normal"
                variant="outlined"
              />
            </FormControl>
            <FormControl className={classes.margin}>
              <Typography variant="h6" id="body2">
                Disponibilidad
            </Typography>
              <TextField
                select
                variant="outlined"
                margin="normal"
                value={available}
                onChange={handleChange}
                input={<OutlinedInput name="available" id="outlined-age-simple" />}
                className={classes.textField}
              >
                <MenuItem value={0}>
                  <em>Ninguno</em>
                </MenuItem>
                <MenuItem value={1}>Disponible</MenuItem>
                <MenuItem value={2}>No Disponible</MenuItem>
              </TextField>
            </FormControl>

            <FormControl className={classes.margin}>
              <Typography variant="h6" id="body2">
                Por Nombre
            </Typography>
              <TextField
                id="outlined-max"
                placeholder="Nombre del producto"
                className={classes.textField}
                value={nameProduct}
                onChange={handleChangeNameProduct}
                
                margin="normal"
                variant="outlined"
              />
            </FormControl>
          </form>
        </Collapse>
      </Paper>
      <Paper className={classes.paper}>
        <ItemGroupComponentToolbar name={"Ordenamiento"} toggleCollapse={handleChangeToggleOrder} />
        <Collapse in={ToggleOrder} timeout="auto" unmountOnExit>

          <div className={classes.tableWrapper}>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={'small'}
            >
              <ItemGroupComponentHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={props.products.length}
              />
              
            </Table>
          </div>
        </Collapse>
      </Paper>
      <Grid container display="flex" justify="center" className={classesGrid.root} spacing={2}>
        {stableSort(newProducts, getSorting(order, orderBy))
          .map((row) => {
            return (
              <Grid item xs={6} sm={4} md={3} lg={2} key={row.id}>
                <SingleItemComponent data={row} />
              </Grid>
            );
          })}
      </Grid>
    </div>
  );
}

export default ItemGroupComponent;