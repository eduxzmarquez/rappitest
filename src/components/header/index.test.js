import React from 'react';
import ReactDOM from 'react-dom';
import Header from './index';
import { categories } from "../../data/categories.json"

it('Header: renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Header categories={categories} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
