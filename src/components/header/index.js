import React from 'react'

// UI
import Collapse from '@material-ui/core/Collapse';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';

import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

// UI ICONS
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import HomeIcon from '@material-ui/icons/Home';
import ShoppingCart from '@material-ui/icons/ShoppingCart'
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import { history } from '../../redux/store'

import SingleCollapsingItemComponent from '../singleCollapsingItemComponent'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  list: {
    width: 250,
  },
  fullList: {
    width: 250,
  },
  appbar: {
    backgroundColor: "#F26245"
  }
}));


function HeaderWithDrawerComponent(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    left: false,
  });
  const [openCategories, setopenCategories] = React.useState(false);

  //const { history } = props
  const toggleDrawer = (side, open) => event => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setState({ ...state, [side]: open });
  };

  const goToRoute = (route) => {
    history.push(route)

  }
  const handleOpenCategories = () => {
    setopenCategories(!openCategories)
  }

  const fullList = side => (
    <div
      className={classes.fullList}
      role="presentation"
    >
      <ListItem button onClick={() => goToRoute("/")}>
        <ListItemIcon>
          <HomeIcon />
        </ListItemIcon>
        <ListItemText primary={"HOME"} />
      </ListItem>
      <Divider />
      <ListItem button onClick={() => goToRoute("/cart")}>
        <ListItemIcon>
            <ShoppingCart />
        </ListItemIcon>
        <ListItemText primary={"SHOPPING CART"} />
      </ListItem>
      <Divider />
      <ListItem onClick={() => { handleOpenCategories() }}>
        <ListItemText primary={"CATEGORIAS"} />
        {openCategories ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Divider />
      <Collapse in={openCategories} timeout="auto" unmountOnExit>
        {props.categories.map((category, index) => (
          <div key={category.id}>
            <SingleCollapsingItemComponent data={category} />
            <Divider />
          </div>
        ))}
      </Collapse>

      <Divider />
    </div>
  );

  return (
    <div>
      <AppBar position="static" className={classes.appbar}>
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="Menu"
            onClick={toggleDrawer('left', true)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Rappi
          </Typography>
          <IconButton edge="start" onClick={() => goToRoute("/")} className={classes.menuButton} color="inherit" aria-label="Menu">
            <HomeIcon />
          </IconButton>
          <IconButton edge="start" onClick={() => goToRoute("cart")} className={classes.menuButton} color="inherit" aria-label="Menu">
            <ShoppingCart />
          </IconButton>
        </Toolbar>
        <SwipeableDrawer
          anchor="left"
          open={state.left}
          onClose={toggleDrawer('left', false)}
          onOpen={toggleDrawer('left', true)}
        >
          {fullList('left')}
        </SwipeableDrawer>
      </AppBar>

    </div>
  );
}

export default HeaderWithDrawerComponent;