import React from 'react';
import ReactDOM from 'react-dom';
import FormDialogUnit from './index';


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<FormDialogUnit  />, div);
  ReactDOM.unmountComponentAtNode(div);
});