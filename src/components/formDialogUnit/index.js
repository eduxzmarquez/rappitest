import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


function FormDialogUnit(props) {
    const [value, setValue] = React.useState(1);

  const { close, open, handleUnitValue } = props



  function handleClose() {
    close()
  }
  function handleConfirm() {
    handleUnitValue(value)
    setValue('')
  }
  function handleChangeValue(value) {
    setValue(value.target.value)
  }



  return (
    <div>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Edicion de Producto</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Ingrese las unidades que desea comprar de este producto.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="unit"
            label="Unidades deseadas"
            type="number"
            fullWidth
            value={value}
            onChange={(v)=>handleChangeValue(v) }
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleConfirm} color="primary">
            Editar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default FormDialogUnit;