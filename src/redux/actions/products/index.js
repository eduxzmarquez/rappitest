import * as types from './constants'

/**
 * Funcion que "compra" todos los items y los saca del Shopping Cart
 */
export function buyAllInCart(){
    return (dispatch)=>{
        localStorage.clear()
        dispatch(setShoppingCartItems([]))
    }
}


/**
 * Funcion que agrega un valor al carrito de compras.
 * @param { object } item - El producto que se va a agregar.
 * @param { number } unit - El valor de unidades que el cliente quiere.
 */
export function addToCart(item, unit) {
    return (dispatch, getState)=>{
        const { productsReducer:{ shoppingCartItems = [] }} = getState()
        let match = shoppingCartItems.find(product=>{
            return product.id === item.id
        })
        if(!match){
            shoppingCartItems.push({id:item.id, unit}) 
            let localData = JSON.stringify(shoppingCartItems)
            localStorage.setItem("ShoppingCart",localData)
            dispatch(setShoppingCartItems(shoppingCartItems))    
        }else{
            let newValue = shoppingCartItems.map(product=>{
                return product.id === item.id ? { id: product.id, unit}: product
            })  
            let localData =  JSON.stringify(newValue)
            localStorage.setItem("ShoppingCart",localData)
            dispatch(setShoppingCartItems(newValue))
        }
    }
}

/**
 * Funcion que elimina el valore que el usuario selecciono.
 * @param { object } item - El producto que se va a eliminar.
 */
export function removeProduct(item) {
    return (dispatch, getState)=>{
        let { productsReducer:{ shoppingCartItems } } = getState()
        let newCartItems = shoppingCartItems.filter(product=>{
            if(product.id === item.id) return false    
            return true
        })
        let localData =  JSON.stringify(newCartItems)
        localStorage.setItem("ShoppingCart",localData)
        dispatch(setShoppingCartItems(newCartItems))
    }
}

/**
 * Funcion que edita los valores que el usuario esta pidiendo.
 * @param { object } item - El producto que se va a editar.
 * @param { number } value - El nuevo valor que unidades que el cliente quiere.
 */
export function editProductUnit(item, value) {
    return (dispatch, getState)=>{
        let { productsReducer:{ shoppingCartItems } } = getState()
        
        let newCartItems = shoppingCartItems.map(product=>{
            
            if(product.id === item.id) return { id: item.id, unit: value}    
            return product
        })
        let localData =  JSON.stringify(newCartItems)
        localStorage.setItem("ShoppingCart",localData)
        dispatch(setShoppingCartItems(newCartItems))
    }
}

/**
 * Funcion  que se encarga de enlistar los productos dependiente del subnivel o nivel principal al q se encuentre
 * @param { number } number - El valor numero por el que se buscara el subnivel
 */
export function setRank(number) {
    return (dispatch, getState)=>{
        const { productsReducer:{ categories , products } } = getState()

        if(number === -1)dispatch(setProducts(products))
        else {

            let newProducts = categories.filter( (category, index, array)=>{
                if( category.id <= number && !array[index+1])return true
                if( category.id <= number && number < array[index+1].id) return true
            return false
        }).map((branch)=>{
            if( branch.id === number) return branch
            let x = findingSubLevel(branch.sublevels, number);
            return x 
        }).map((node, index, array)=>{
            if( !Array.isArray(node.sublevels)) return node
            return getCategoryListflatted(node)      
        }).map((CategoriesSelected, index, arr)=>{
            if(Array.isArray(CategoriesSelected)){
                return CategoriesSelected.map(singleCategory=>{
                    return _filterProducts(singleCategory, products)
                })
            }else{
                return _filterProducts(CategoriesSelected, products)
            }  
        })
        newProducts = flattenSimple(newProducts[0])
        dispatch(setProducts(newProducts))
    }
    }
}

// ------------------ Helpers ---------------

/**
 * 
 * @param { number } id 
 * @param { Array } products 
 */
const _filterProducts = ({ name, id}, products)=>{
    return products.filter((product, index)=>{
        return id === product.sublevel_id
    }).map(productfiltered=>({ ...productfiltered, categoryName:name }))
}

/**
 *Buscando el nodo donde se encuentra la categoria seleccionada de forma recursiva
 * 
 * @param { Array } arr     - De tipo Array o arbol
 * @param { number } number - De tipo numerico; es el id seleccionado
 */
const findingSubLevel = (arr = [], number)=>{
    for(var sub of arr){
        if(sub.id === number) return sub
        else if(!!sub.sublevels) return findingSubLevel(sub.sublevels, number)
    }
}
/**
 * Aplanamos el nodo y sus sublevels con un metodo recursivo para un mejor manejo.
 * @param { Array } arr La cabeza del arbol que se desea hacer Flat
 */
const getCategoryListflatted = (arr) => {
    let arrays = Array.isArray(arr.sublevels) ? arr.sublevels : [];
    let result = []
    for( let item of arrays){
        let h = getCategoryListflatted(item)
        result = result.concat(h)
    }
    let resultado = Array.isArray(arr.sublevels) ? result.concat([arr]) : [arr]
    return resultado
}

/**
 * Simple Flat
 * @param { Array } arr - Es un vector de vectores
 */
const flattenSimple = (arr)=>{
    return arr.reduce((acc, val) => Array.isArray(val) ? acc.concat(flattenSimple(val)) : acc.concat(val), []);
 }



//---------------- FUNCIONES PURAS ---------------

const setProducts =(payload)=>({
    type:types.SETTING_PRODUCTS,
    payload
})

const setShoppingCartItems =(payload)=>({
    type:types.SETTING_SHOPPINGCARTITEMS,
    payload
})
