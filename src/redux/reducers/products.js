import * as types from '../actions/products/constants'
import { products } from '../../data/products.json'
import { categories } from '../../data/categories.json'

const initialState = {
  products: products,
  productsListed: products,
  categories,
  shoppingCartItems: 
  JSON.parse(localStorage.getItem("ShoppingCart")) ? 
  JSON.parse(localStorage.getItem("ShoppingCart")) : []
}

export default function productsReducer(state = initialState, action) {
  switch (action.type) {
    case types.SETTING_PRODUCTS:
      return { ...state, productsListed: action.payload }

    case types.SETTING_SHOPPINGCARTITEMS:
      return { ...state, shoppingCartItems: action.payload }

    default:

  }

  // Por ahora, no maneja ninguna acción
  // y solo devuelve el estado que recibimos.
  return state
}