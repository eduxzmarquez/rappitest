## Título del Proyecto

### Frontend Developer Challenge



## Comenzando 🚀

El proyecto se ejecuta con el comando "npm start" en la raiz del proyecto.
Solo es abrir el sistema de comandos en la raiz y escribir el comando y presionar enter.

## Pre-requisitos 📋

Es necesario tener instalado Node.js para la gestion de paquetes de npm.

## Instalación 🔧

Luego de haber clonado el repositorio, se debe ejecutar el comando "npm install" para descargar todas las depencias necesarias en el proyecto.
(en su defecto "npm i")


## Ejecutando las pruebas ⚙️

**Al entrar en la applicacion se encontrara directamente con el listado de productos y sobre ellos, las opciones de ordenamiento y filtrado que nos ofrece la aplicacion; podemos probar con numeros reales los diferentes metodos y en tiempo real se estaran reflejando en los productos mostrados en la parte inferior.**


![Alt text](/src/assets/readme/screen1.png)


**Adicionalmente tenemos un filtrado de productos disponibles y no disponibles por colores; los productos con color rojizo-rosado pueden ser filtrador por la opcion "No Disponible", sin embargo, pueden ser seleccionados y comprados como efecto del ejemplo**


![Alt text](/src/assets/readme/screen7.png)


**Si abrimos el Menu lateral (Drawer) tenemos dos navegadores como primeros dos botones, seguidamente tenemos el listado de categorias cargadas de forma dinamica. Estan ordenadas por desplegables que nos muestras los subniveles de profundida de cada una de las categorias; a medida que navegamos a traves de los subniveles, la applicacion esta buscando y ordenando en tiempo real los productos mostrados en el home.**


![Alt text](/src/assets/readme/screen2.png)
![Alt text](/src/assets/readme/screen5.png)


**Cuando querramos añadir un producto al carro de compras, presionamos sobre el boton "Comprar" de las tarjetas de los productos. Nos pedira el numero de productos que queremos comprar y posteriormente lo agregara al carro de compras.**

**Al comprar un producto, si ya estaba en el carrito de compras, simplemente va a reemplazar el nuevo valor de unidades que se quiere.**

**En el carro de compras, tenemos el listado de los productos que queremos comprar enlistados con sus propiedades. Tenemos por cada uno de los productos, acciones en la izquierda de cada producto para tanto editarlos como eliminarlos. Podemos editar las unidades requeridas.**


![Alt text](/src/assets/readme/screen4.png)


**El carro de compras tiene persistencia en los datos, por lo que al cerrar la aplicacion y volver a entrar, seguiran estando los productos.**
 

## Analice las pruebas end-to-end 🔩

Para hacer realizar las pruebas unitarias, vamos a la raiz del proyecto y corremos el comando "npm test", donde mostrara los resultados de los componentes utilizados en el proyecto.



## Autores ✒️

**Eduardo Marquez - Desarrollador Web**




# *PROYECTO DESARROLLADO EN REACT CON REDUX*